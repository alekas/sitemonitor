package com.elykinnovation.SiteMonitor;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

public class Database
{
    private final String framework = "embedded";
    private final String driver = "org.apache.derby.jdbc.EmbeddedDriver";
    private final String protocol = "jdbc:derby:";
    public final String dbName = "SiteMonitor";
    private ArrayList<Statement> statements = new ArrayList<Statement>();
    
    private Connection conn = null;
    
	public Database()
	{
		loadDriver();
        Statement s = null;
        ResultSet rs = null;
        try
        {
            Properties props = new Properties();

            conn = DriverManager.getConnection(protocol + dbName
                    + ";create=true", props);
            
            //conn.setAutoCommit(false);

            s = conn.createStatement();
            statements.add(s);
            
            DatabaseMetaData dmd = conn.getMetaData();
            rs = dmd.getTables(null, "APP", null, new String[] {"TABLE"});
            
            if (!rs.next()) 
            {
            	setupDatabase();
            }
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
	}

    
    private void setupDatabase() throws SQLException
	{
    	System.out.println("Database not setup. Setting up...");
    	executeUpdate("CREATE TABLE sites (id INT NOT NULL GENERATED ALWAYS AS IDENTITY, name VARCHAR(255), url VARCHAR(255), frequency INT NOT NULL, search_string VARCHAR(255), PRIMARY KEY (id))");
    	executeUpdate("CREATE TABLE requests (id INT NOT NULL GENERATED ALWAYS AS IDENTITY, sites_id INT NOT NULL, response_time DECIMAL(12,4), timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, headers LONG VARCHAR, body LONG VARCHAR, response_code INT, response_message VARCHAR(255), PRIMARY KEY (id))");
    	executeUpdate("CREATE TABLE emails (id INT NOT NULL GENERATED ALWAYS AS IDENTITY, email_address VARCHAR(255), description VARCHAR(255), PRIMARY KEY (id))");
	}
    
    public void executeUpdate(String sql) throws SQLException
    {
    	Statement s = conn.createStatement();
    	s.executeUpdate(sql);
    	statements.add(s);
    }
    
    public ResultSet execute(String sql) throws SQLException
    {
    	Statement s = conn.createStatement();
    	return s.executeQuery(sql);
    }
    
    public void executePreparedUpdate(String sql, Object... args) throws SQLException
    {
    	PreparedStatement s = conn.prepareStatement(sql);
    	for(int i=0; i<args.length; i++)
    	{
    		if(args[i] instanceof String)
    		{
    			s.setString(i+1, (String)args[i]);
    			continue;
    		}
    		if(args[i] instanceof Integer)
    		{
    			s.setInt(i+1, (Integer)args[i]);
				continue;
    		}
    		if(args[i] instanceof String)
    		{
    			s.setDouble(i+1, (Double)args[i]);
    			continue;
    		}
    		if(args[i] instanceof Float)
    		{
    			s.setFloat(i+1, (Float)args[i]);
    			continue;
    		}
    	}
    	
    	statements.add(s);
    	s.executeUpdate();
    }
    
    public ResultSet executePrepared(String sql, Object... args) throws SQLException
    {
    	PreparedStatement s = conn.prepareStatement(sql);
    	for(int i=0; i<args.length; i++)
    	{
    		if(args[i] instanceof String)
    		{
    			s.setString(i+1, (String)args[i]);
    			continue;
    		}
    		if(args[i] instanceof Integer)
    		{
    			s.setInt(i+1, (Integer)args[i]);
				continue;
    		}
    		if(args[i] instanceof String)
    		{
    			s.setDouble(i+1, (Double)args[i]);
    			continue;
    		}
    		if(args[i] instanceof Float)
    		{
    			s.setFloat(i+1, (Float)args[i]);
    			continue;
    		}
    	}
    	
    	statements.add(s);
    	return s.executeQuery();
    }


	private void loadDriver()
    {
        try {
            Class.forName(driver).newInstance();
            System.out.println("Loaded the appropriate driver");
        } catch (ClassNotFoundException cnfe) {
            System.err.println("\nUnable to load the JDBC driver " + driver);
            System.err.println("Please check your CLASSPATH.");
            cnfe.printStackTrace(System.err);
        } catch (InstantiationException ie) {
            System.err.println(
                        "\nUnable to instantiate the JDBC driver " + driver);
            ie.printStackTrace(System.err);
        } catch (IllegalAccessException iae) {
            System.err.println(
                        "\nNot allowed to access the JDBC driver " + driver);
            iae.printStackTrace(System.err);
        }
    }
	
	public void close()
	{
		try
		{
			conn.close();
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
