package com.elykinnovation.SiteMonitor;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Website
{
	private String name, URL, search_string;
	private int frequency;
	public WebsiteConnection conn;
	
	
	public Website(String name, String URL, int frequency, String search_string)
	{
		this.name = name;
		this.URL = URL;
		this.search_string = search_string;
		this.frequency = frequency;
		try
		{
			conn = new WebsiteConnection(URL);
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		
		SiteMonitor.scheduler.scheduleAtFixedRate(new WebsiteRunnable(this, this.conn), 0, frequency, TimeUnit.SECONDS);
	}

	public String getURL()
	{
		return URL;
	}

	public void setURL(String URL)
	{
		this.URL = URL;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getSearchString()
	{
		return search_string;
	}

	public void setSearchString(String search_string)
	{
		this.search_string = search_string;
	}

	public int getFrequency()
	{
		return frequency;
	}

	public void setFrequency(int frequency)
	{
		this.frequency = frequency;
	}
}
