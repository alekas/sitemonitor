package com.elykinnovation.SiteMonitor;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.IOUtils;

public class WebsiteConnection
{
	private String urlString = null;
	private URL url = null;
	private Set<Entry<String, List<String>>> headers = null;
	private String body = null;
	private int responseCode;
	private String responseMessage;
	private double responseTime;
	
	public WebsiteConnection(String urlString) throws MalformedURLException
	{
		this.urlString = urlString;
		url = new URL(urlString);
	}
	
	public boolean isAlive() throws IOException
	{
		HttpURLConnection conn = null;
		long start = System.nanoTime();
		conn = (HttpURLConnection)url.openConnection();
		
		Map<String, List<String>> headerfields = conn.getHeaderFields();
		this.headers = headerfields.entrySet(); 
		this.responseTime = (System.nanoTime()-start)/1000000000.0;
		
		InputStream in = conn.getInputStream();
		String encoding = conn.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		
		this.body = IOUtils.toString(in, encoding);
		
		this.responseCode = conn.getResponseCode();
		this.responseMessage = conn.getResponseMessage();
		
		switch(conn.getResponseCode())
		{
			case 200:
			case 302:
			case 301:
				return true;
		}
		return false;
	}

	public String getResponseMessage()
	{
		return responseMessage;
	}

	public int getResponseCode()
	{
		return responseCode;
	}

	public String getBody()
	{
		return body;
	}
	public Set<Entry<String, List<String>>> getHeaders()
	{
		return headers;
	}
	
	public String getHeaderString()
	{
		String str = "";
		for (Map.Entry<String, List<String>> entry : headers) {
			str+=entry.getKey() + ": " + entry.getValue()+"\\n";
		}
		return str;
	}

	public double getResponseTime()
	{
		return responseTime;
	}

	public String getURL() {
		return this.urlString;
	}
}
