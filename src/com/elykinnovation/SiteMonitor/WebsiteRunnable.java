package com.elykinnovation.SiteMonitor;

import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public class WebsiteRunnable implements Runnable 
{
	private Website website;
	private WebsiteConnection conn;
	
	public WebsiteRunnable(Website website, WebsiteConnection conn)
	{
		this.website = website;
		this.conn = conn;
	}
	
	@Override
	public void run() 
	{
		try
		{
			System.out.println("Checking "+conn.getURL());
			boolean is_alive = conn.isAlive();
			String body = conn.getBody();
			Set<Entry<String, List<String>>> headers = conn.getHeaders();
			String response_message = conn.getResponseMessage();
			int response_code = conn.getResponseCode();
			
			if(response_code!=200 || !response_message.equals("OK") || !body.contains(website.getSearchString()))
				this.sendFailureEmail();
			else
				System.out.println("Alive!");
				
			
		}
		catch (IOException e)
		{
			sendFailureEmail();
			e.printStackTrace();
		}
	}
	
	public void sendFailureEmail()
	{
		try {
			RackspaceMail.Send("errors@elykinnovation.com", "YgMtpuLOPcYqtYJa", "alekas@elykinnovation.com", "[WEBSITE DOWN] "+this.website.getName(), this.website.getName()+" ("+this.website.getURL()+") is not available.<br>Response Code: "+this.conn.getResponseCode()+"<br>Response Message: "+this.conn.getResponseMessage()+"<br>Body: <pre>"+org.apache.commons.lang3.StringEscapeUtils.escapeHtml4(this.conn.getBody())+"</pre>");
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
