package com.elykinnovation.SiteMonitor;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public class SiteMonitor
{
    /**
	 * @param args
	 */
	
	public static Database db;
	public static ArrayList<Website> websites = new ArrayList<Website>();
	public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(50);
	
	public static void main(String[] args)
	{
        
		db = new Database();
		boolean exit = false;
				
		try
		{
			ResultSet rs = db.execute("SELECT * FROM APP.sites ORDER BY URL");
			while(rs.next())
			{
				Website website = new Website(rs.getString("name"), rs.getString("URL"), rs.getInt("frequency"), rs.getString("search_string"));
				websites.add(website);
			}
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
		while(!exit)
		{
			drawMenu();
			String input = getInput();
			switch(input)
			{
				case "1":
					System.out.println("Enter site name, url, frequency in seconds, string to search for [or q to cancel]");
					
					String input_str = getInput();
					if(!input_str.equals("q"))
					{
						
						Pattern p = Pattern.compile("^(.*?), *(.*?), *(.*?), *(.*?)$");
						System.out.println("\""+input_str+"\"");
						Matcher m = p.matcher(input_str);
						m.matches();
						
						String name = m.group(1); 
						String url = m.group(2);
						Integer frequency = Integer.valueOf(m.group(3));
						String search_string = m.group(4);
						System.out.println("Creating site "+name+" @ "+url+" to check every "+frequency+" seconds for the string: \""+search_string+"\"");
						try
						{
							db.executePreparedUpdate("INSERT INTO sites (name, URL, frequency, search_string) VALUES (?, ?, ?, ?)", name, url, frequency, search_string);
						}
						catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				break;
				case "2":
					System.out.print("Enter site search query: ");
					String query = getInput();
					try
					{
						ResultSet rs = db.executePrepared("SELECT * FROM sites WHERE name LIKE ? OR url LIKE ?", "%"+query+"%", "%"+query+"%");
						System.out.printf("%-3s  %-45s  %-45s %-10s\n", "ID", "Name", "URL", "Frequency", "Search String");
						System.out.println("---  ---------------------------------------------  --------------------------------------------  ---------  --------------------------------------------");
						while(rs.next())
						{
							System.out.printf("%-3s  %-45s  %-45s %-10s %-45s\n", rs.getInt("id"), rs.getString("name"), rs.getString("URL"), rs.getInt("frequency")+"s", rs.getString("search_string"));
						}
						System.out.print("Enter ID to edit [q to cancel]? ");
						String to_edit = getInput();
						if(!to_edit.equals("q"))
						{
							System.out.println("Enter site name, url, frequency in seconds, search string [q to cancel, r to remove site]");
							String update_string = getInput();
							if(!update_string.equals("q") && !update_string.equals("r"))
							{
								
								Pattern p = Pattern.compile("^(.*?), *(.*?), *(.*?), *(.*?)$");
								Matcher m = p.matcher(update_string);
								m.matches();
								
								String name = m.group(1); 
								String url = m.group(2);
								Integer frequency = Integer.valueOf(m.group(3));
								String search_string = m.group(4);
								
								System.out.println("Updating site "+to_edit+" to "+name+" @ "+url+" to check every "+frequency+" seconds for the string: \""+search_string+"\"");
								try
								{
									db.executePreparedUpdate("UPDATE sites SET name=?, URL=?, frequency=?, search_string=? WHERE id=?", name, url, frequency, search_string, Integer.valueOf(to_edit));
									System.out.println("Record updated successfully.");
								}
								catch (SQLException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else if(update_string.equals("r"))
								db.executePreparedUpdate("DELETE FROM sites WHERE id = ?", Integer.valueOf(to_edit));
						}
					}
					catch (SQLException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				break;
				case "3":
					try
					{
						ResultSet rs = db.execute("SELECT * FROM APP.sites ORDER BY URL");
						System.out.printf("%-3s  %-45s  %-45s %-11s %-45s\n", "ID", "Name", "URL", "Frequency", "Search String");
						System.out.println("---  ---------------------------------------------  --------------------------------------------  ----------  --------------------------------------------");
						
						while(rs.next())
						{
							System.out.printf("%-3s  %-45s  %-45s %-11s %-45s\n", rs.getInt("id"), rs.getString("name"), rs.getString("URL"), rs.getInt("frequency")+"s", rs.getString("search_string"));
						}
						
					}
					catch(SQLException e)
					{
						e.printStackTrace();
					}
				break;
				case "4":
					
				break;
				case "5":
					System.out.println("Enter email address, description [or q to cancel]");
					
					input_str = getInput();
					if(!input_str.equals("q"))
					{
						
						Pattern p = Pattern.compile("^(.*?), *(.*?)$");
						System.out.println("\""+input_str+"\"");
						Matcher m = p.matcher(input_str);
						m.matches();
						
						String email = m.group(1); 
						String description = m.group(2);
						System.out.println("Creating email "+email+": "+description);
						try
						{
							db.executePreparedUpdate("INSERT INTO emails (email_address, description) VALUES (?, ?)", email, description);
						}
						catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				break;
				case "6":
					System.out.print("Enter email search query: ");
					query = getInput();
					try
					{
						ResultSet rs = db.executePrepared("SELECT * FROM emails WHERE email_address LIKE ? OR description LIKE ?", "%"+query+"%", "%"+query+"%");
						System.out.printf("%-3s  %-45s  %-45s\n", "ID", "Email", "Description");
						System.out.println("---  ---------------------------------------------  --------------------------------------------");
						while(rs.next())
						{
							System.out.printf("%-3s  %-45s  %-45s\n", rs.getInt("id"), rs.getString("email_address"), rs.getString("description"));
						}
						System.out.print("Enter ID to edit [q to cancel]? ");
						String to_edit = getInput();
						if(!to_edit.equals("q"))
						{
							System.out.println("Enter email, description [q to cancel, r to remove email]");
							String update_string = getInput();
							if(!update_string.equals("q") && !update_string.equals("r"))
							{
								
								Pattern p = Pattern.compile("^(.*?), *(.*?)$");
								Matcher m = p.matcher(update_string);
								m.matches();
								
								String email = m.group(1); 
								String description = m.group(2);
								
								System.out.println("Updating email "+to_edit+" to "+email+": "+description);
								try
								{
									db.executePreparedUpdate("UPDATE emails SET email_address=?, description=? WHERE id=?", email, description, Integer.valueOf(to_edit));
									System.out.println("Record updated successfully.");
								}
								catch (SQLException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else if(update_string.equals("r"))
								db.executePreparedUpdate("DELETE FROM sites WHERE id = ?", Integer.valueOf(to_edit));
						}
					}
					catch (SQLException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				break;
				case "7":
					try
					{
						ResultSet rs = db.execute("SELECT * FROM APP.emails ORDER BY email_address");
						System.out.printf("%-3s  %-45s  %-45s\n", "ID", "Email", "Description");
						System.out.println("---  ---------------------------------------------  --------------------------------------------");
						
						while(rs.next())
						{
							System.out.printf("%-3s  %-45s  %-45s\n", rs.getInt("id"), rs.getString("email_address"), rs.getString("description"));
						}
						
					}
					catch(SQLException e)
					{
						e.printStackTrace();
					}
				break;
				case "q":
					exit=true;
			}
		}
		/*WebsiteConnection conn = null;
		try
		{
			conn = new WebsiteConnection("http://www.elykinnovation.com");
		}
		catch (MalformedURLException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(conn != null)
		{
			try
			{
				boolean isAlive = conn.isAlive();
				if(isAlive)
				{
					System.out.println("Website is up. Responded in "+(conn.getResponseTime()));
				}
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
		
		//db.close();
	}

	public static void drawMenu()
	{
		System.out.println("1) Add site");
		System.out.println("2) Edit site");
		System.out.println("3) View sites");
		System.out.println("4) View history");
		System.out.println("5) Add email address");
		System.out.println("6) Edit email address");
		System.out.println("7) View email addresses");
		System.out.print("Selection [q to exit]: ");
	}
	
	public static String getInput()
	{
		Scanner input = new Scanner( System.in );
		return input.nextLine();
	}
}
